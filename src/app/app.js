angular.module('app', ['templates-app'])

  .controller('appCtrl', function ($scope) {
    $scope.message = "Hello World";
    $scope.user = {
      name: 'Arup',
      address: {
        flatNo: 17,
        building: 'Sri Rajarajeswari Nilaya',
        street: 'Immadihalli-Nagondanahalli main road',
        at: 'Greenfield',
        city: 'mogaluru',
        country: 'Kazakhistan'
      },
      friends: [
        'Nadia',
        'Pandia',
        'Gobara'
      ]
    };

    $scope.knightMe = function(user){
      user.rank = "Knight";
    };
  })

  .directive('userInfoCard', function () {
    return {
      templateUrl: 'userInfoCard.tpl.html',
      restrict: 'E'
    };
  });